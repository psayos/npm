#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <chrono>
#include <set>
#include <map>

using namespace std;

int IND = -1; // Valor para indicar que no hubo solución.

// Información de la instancia a resolver.
int n, M;
vector<int> B;
vector<int> C;

// i: posicion del elemento a considerar en este nodo.
// b: suma de los beneficios de los elementos seleccionados hasta este nodo.
// c: suma de los niveles de contagio de los elementos seleccionados hasta este nodo.
int FB(int i, int b, int c)
{
    // Caso base.
    if (i >= n){
        if(c > M){
            return -1;
        } else{
            return b;
        }
    }
    // Recursión.
    return max(FB(i+1, b, c), FB(i+2, b+B[i], c+C[i]));
}

// i: posicion del elemento a considerar en este nodo.
// k: cantidad de elementos seleccionados hasta este nodo.
// w: suma de los elementos seleccionados hasta este nodo.
bool poda_factibilidad = true; // define si la poda por factibilidad esta habilitada.
bool poda_optimalidad = true; // define si la poda por optimalidad esta habilitada.
int mb = 0; // Mejor solucion hasta el momento.
int BT(int i, int b, int c, int br)
{
    if(b > mb){ // Cambia mejor solucion
        mb = b;
    }

    // Caso base.

    if(i >= n){
        if(c > M){
            return IND;
        } else{
            return b;
        }
    }
    // Poda por factibilidad.
    if (poda_factibilidad && c > M) return IND;

    // Poda por optimalidad.
    if (poda_optimalidad && b + br <= mb) return IND; // Definir br en el main.

    // Recursión.
    return max(BT(i+1, b, c, br - B[i]), BT(i+2, b+B[i], c+C[i], br - B[i]));
}

vector<vector<int>> MT; // Memoria de PD.
const int UNDEFINED = -1;

int PD(int i, int c)
{
    if( i >= n){
        return 0;
    }
    if (c >= M) return IND;

    if (MT[i][c] == UNDEFINED) {
        MT[i][c] = max(PD(i + 1, c), B[i] + PD(i + 2, c + C[i]));
    }
    return MT[i][c];
}

// Recibe por parámetro qué algoritmos utilizar para la ejecución separados por espacios.
// Imprime por clog la información de ejecución de los algoritmos.
// Imprime por cout el resultado de algun algoritmo ejecutado.
int main(int argc, char** argv)
{
    // Leemos el parametro que indica el algoritmo a ejecutar.
    map<string, string> algoritmos_implementados = {
            {"FB", "Fuerza Bruta"}, {"BT", "Backtracking con podas"}, {"BT-F", "Backtracking con poda por factibilidad"},
            {"BT-O", "Backtracking con poda por optimalidad"}, {"DP", "Programacion dinámica"}
    };

    // Verificar que el algoritmo pedido exista.
    if (argc < 2 || algoritmos_implementados.find(argv[1]) == algoritmos_implementados.end())
    {
        cerr << "Algoritmo no encontrado: " << argv[1] << endl;
        cerr << "Los algoritmos existentes son: " << endl;
        for (auto& alg_desc: algoritmos_implementados) cerr << "\t- " << alg_desc.first << ": " << alg_desc.second << endl;
        return 0;
    }
    string algoritmo = argv[1];

    // Leemos el input.
    cin >> n >> M;
    S.assign(n, 0);
    for (int i = 0; i < n; ++i) cin >> B[i]; // Vector de beneficios
    for (int i = 0; i < n; ++i) cin >> C[i]; // Vector de nivel de contagios

    int br = 0; // La sumatoria de los beneficios.
    for(int i = 0, i < n; i++){
        br+= B[i];
    }

    // Ejecutamos el algoritmo y obtenemos su tiempo de ejecución.
    int optimum;
    optimum = INFTY;
    auto start = chrono::steady_clock::now();
    if (algoritmo == "FB")
    {
        optimum = FB(0, 0, 0);
    }
    else if (algoritmo == "BT")
    {
        int mb = 0;
        poda_optimalidad = poda_factibilidad = true;
        optimum = BT(0, 0, 0, br);
    }
    else if (algoritmo == "BT-F")
    {
        int mb = 0;
        poda_optimalidad = false;
        poda_factibilidad = true;
        optimum = BT(0, 0, 0, br);
    }
    else if (algoritmo == "BT-O")
    {
        int mb = 0;
        poda_optimalidad = true;
        poda_factibilidad = false;
        optimum = BT(0, 0, 0, br);
    }
    else if (algoritmo == "DP")
    {
        // Precomputamos la solucion para los estados.
        MT = vector<vector<int>>(n+1, vector<int>(M+1, UNDEFINED));
        for (int i = 0; i < n+1; ++i)
            for (int j = 0; j < W+1; ++j)
                PD(i, j);

        // Obtenemos la solucion optima.
        optimum = PD(0, 0);
    }
    auto end = chrono::steady_clock::now();
    double total_time = chrono::duration<double, milli>(end - start).count();

    // Imprimimos el tiempo de ejecución por stderr.
    clog << total_time << endl;

    // Imprimimos el resultado por stdout.
    cout << (optimum == IND ? -1 : optimum) << endl;
    return 0;
}
